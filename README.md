Jesus Mode is a script that allows you to be able to walk and drive on water, just like Jesus!
This is an old mod converted from Scripthook (C++) to .NET Scripthook (C#). I converted it because a lot of people couldn't get the Scripthook version to work.

How to install:
First install .NET Scripthook, then copy JesusMode.net.dll and JesusMode.ini to the scripts folder in your GTA IV directory.

Controls:
Right-CTRL + J = Toggle Jesus Mode
-Keys are changeable in the ini file

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
