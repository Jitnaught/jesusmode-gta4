﻿using GTA;
using System;
using System.IO;
using System.Windows.Forms;

namespace JesusMode
{
    public class JesusMode : Script
    {
        bool enabled = false, useHoldKey;
        GTA.Object floatingObj; //"nj05_oiltnkbig"
        Keys togglePressKey, toggleHoldKey;

        public JesusMode()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("toggle_press_key", "keys", Keys.J);
                Settings.SetValue("toggle_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("use_hold_key", "keys", true);
                Settings.Save();
            }

            togglePressKey = Settings.GetValueKey("toggle_press_key", "keys", Keys.J);
            toggleHoldKey = Settings.GetValueKey("toggle_hold_key", "keys", Keys.RControlKey);
            useHoldKey = Settings.GetValueBool("use_hold_key", "keys", true);

            while (!isInGame()) Wait(500);

            floatingObj = CreateTheObject();

            floatingObj.Visible = false;
            floatingObj.Collision = false;

            KeyDown += JesusMode_KeyDown;
            Interval = 250;
            Tick += JesusMode_Tick;
        }

        private void JesusMode_Tick(object sender, EventArgs e)
        {
            if (enabled)
            {
                if (Game.Exists(floatingObj))
                {
                    Vector3 playerPos = Player.Character.Position, posToPlaceObj = new Vector3(playerPos.X, playerPos.Y, -9.3f);
                    if (Math.Abs(playerPos.Z - posToPlaceObj.Z) < 50.0f)
                    {
                        floatingObj.Visible = false;
                        floatingObj.Collision = true;
                        floatingObj.Position = posToPlaceObj;
                    }
                }
                else floatingObj = CreateTheObject();
            }
            else
            {
                if (Game.Exists(floatingObj))
                {
                    floatingObj.Visible = false;
                    floatingObj.Collision = false;
                }
                else floatingObj = CreateTheObject();
            }
        }

        private void JesusMode_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(toggleHoldKey)) && e.Key == togglePressKey)
            {
                Subtitle("Jesus Mode is now " + ((enabled = !enabled) ? "enabled" : "disabled"));
            }
        }


        private void Subtitle(string text, int time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", text, time, 1);
        }

        private bool isInGame()
        {
            if (Game.Exists(Player.Character) && Player.Character.isAlive && GTA.Native.Function.Call<bool>("IS_SCREEN_FADED_IN") && !GTA.Native.Function.Call<bool>("IS_SCREEN_FADED_OUT") && !GTA.Native.Function.Call<bool>("IS_SCREEN_FADING_IN")) return true;
            return false;
        }

        private GTA.Object CreateTheObject()
        {
            GTA.Object tempObj = World.CreateObject("nj05_oiltnkbig", Vector3.Zero);
            for (byte i = 0; i < 5; i++)
            {
                if (Game.Exists(tempObj)) break;
                Wait(500);
            }
            tempObj.FreezePosition = true;

            return tempObj;
        }
    }
}
